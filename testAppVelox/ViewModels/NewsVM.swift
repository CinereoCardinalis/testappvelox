//
//  NewsVM.swift
//  testAppVelox
//
//  Created by MAC OS on 07/10/2019.
//  Copyright © 2019 CinereoCardinalis. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import XMLParsing


class NewsVM {
    
    typealias Item = RSSVestiRM.Channel.Item

    private let disposeBug = DisposeBag()
    
    //MARK: -
    
    let rssItems = ReplaySubject<[Item]>.create(bufferSize: 1)
    let currentItem = BehaviorSubject<Item?>(value: nil)
    
    let refreshing = ReplaySubject<Bool>.create(bufferSize: 1)
    
    let errorMessages = PublishSubject<ErrorMessageLM>()
    
    //MARK: -
    
    init() {
        Logging.URLRequests = { _ in return false }
    }
    
    //MARK: -
    
    //Загрузка новостей из сети
    func loadRss() {
        
        refreshing.onNext(true)
        
        let urlString = "https://www.vesti.ru/vesti.rss"
        let request = URLRequest(url: URL(string: urlString)!)
        
        URLSession.shared.rx
            .data(request: request)
            .asSingle()
            .map({ data -> RSSVestiRM in
                let formatter = DateFormatter()
                formatter.dateFormat = "E, dd MMM yyyy HH:mm:ss Z"
                let decoder = XMLDecoder()
                decoder.dateDecodingStrategy = .formatted(formatter)
                return try decoder.decode(RSSVestiRM.self, from: data)
            })
            .map({ $0.channel.items })
            .subscribe({ [weak self] in
                switch $0 {
                case .success(let elem): self?.rssItems.onNext(elem)
                case .error(let error):
                    let messageModel = ErrorMessageLM(text: error.localizedDescription)
                    self?.errorMessages.onNext(messageModel)
                }
                self?.refreshing.onNext(false)
            })
            .disposed(by: disposeBug)
    }
    
    //Выбор элемента
    func selectItem(index item: Int) {
        
        self.rssItems
            .take(1)
            .map({ $0[item] })
            .bind(onNext: { self.currentItem.onNext($0) })
            .disposed(by: disposeBug)
    }
    
    //Отмена выбора элемента
    func deselectItem() {
        self.currentItem.onNext(nil)
    }
    
    //Загрузка для выбранного изображения из сети
    func loadImageDataForCurrentItem() -> Observable<Data> {
        return currentItem
            .take(1)
            .flatMap({ (item) -> Observable<Data> in
                guard let url = item?.enclosure.first?.url else {
                    return Observable<Data>.empty()
                }
                let request = URLRequest(url: url)
                return URLSession.shared.rx.data(request: request)
            })
    }
    
    
}

//
//  Router.swift
//  testAppVelox
//
//  Created by MAC OS on 07/10/2019.
//  Copyright © 2019 CinereoCardinalis. All rights reserved.
//

import UIKit.UIWindow

infix operator ??=
fileprivate func ??=<T>(left: inout T?, right: T) {
    if left == nil {
        left = right
    }
}

//Класс предназначен для единообразной иницилизации приложения с учетом изменений в ios13
class Router {
    
    static let shared = Router()
    
    private var window: UIWindow?
    private var navigationController: UINavigationController!
    
    //MARK: -
    
    private init() {}
    
    //MARK: -
    
    @available(iOS, obsoleted: 13)
    func initial(application: UIApplication) {
        
        #if DEBUG
        print("Start with AppDelegate")
        #endif
        
        window ??= UIWindow(frame: UIScreen.main.bounds)
        showInitialViewController()
    }
    
    @available(iOS 13, *)
    func initial(scene: UIScene) {
        
        #if DEBUG
        print("Start with SceneDelegate")
        #endif
        
        guard let windowScene = scene as? UIWindowScene else { return }
        window ??= UIWindow(windowScene: windowScene)
        showInitialViewController()
    }
    
    private func showInitialViewController() {
        guard let window = window else { return }
        let vc = newsListViewController()
        navigationController = UINavigationController(rootViewController: vc)
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
    }
}

//MARK: - Public Interface

extension Router {
    

    
    func changeRoot(_ vc: UIViewController) {
        navigationController.viewControllers = [vc]
    }
    
}

//MARK: - Private
private extension Router {
    
    func newsListViewController() -> UIViewController {
        return TableViewController()
    }
    
}

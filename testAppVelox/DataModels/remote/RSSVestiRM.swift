//
//  RSSVestiRM.swift
//  testAppVelox
//
//  Created by MAC OS on 07/10/2019.
//  Copyright © 2019 CinereoCardinalis. All rights reserved.
//

import Foundation


//Модель данных для стукруры rss
struct RSSVestiRM: Decodable {
    
    let channel: Channel
    
    struct Channel: Decodable {
        
        enum CodingKeys: String, CodingKey {
            case  items = "item"
        }
        
        let items: [Item]
        
        struct Item: Decodable {
            
            enum CodingKeys: String, CodingKey {
                case title, pubDate, category, enclosure
                case fullText = "yandex:full-text"
            }
            
            let title: String
            let pubDate: Date
            let category: String
            let enclosure : [Enclosure]
            let fullText: String
            
            func pubDateString(format: String = "E, dd MMM yyyy HH:mm") -> String? {
                let formatter = DateFormatter()
                formatter.dateFormat = format
                return formatter.string(from: self.pubDate)
            }
            
            struct Enclosure: Decodable {
                let url: URL
            }
        }
    }
}







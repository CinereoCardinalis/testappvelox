//
//  TableViewController.swift
//  testAppVelox
//
//  Created by MAC OS on 07/10/2019.
//  Copyright © 2019 CinereoCardinalis. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class TableViewController: UIViewController {
    
    //MARK: - Variables
    
    private let disposeBag = DisposeBag()
    
    private let tableView = UITableView()
    private let refreshControl = UIRefreshControl()
    private let searchBar = UISearchBar()
    private var searchButton = UIBarButtonItem(barButtonSystemItem: .search, target: nil, action: nil)
    
    var viewModel: NewsVM? = NewsVM()
    
    //Промежуточный субъект, для распространения данных таблицы
    //Он нужен для возможности отображать отфильтрованный список новостей, помимо не отфильтрованного
    private let tableViewData = PublishSubject<[NewsVM.Item]>()

    //MARK: - View Controller
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureUI()
        bindsViewModel()
        bindsUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        viewModel?.loadRss()
    }
    
    //MARK: - Private
    
    private func configureUI() {
        self.view.backgroundColor = .white
        
        self.view.addSubview(tableView)
        self.title = "News"
        
        tableView.backgroundColor = .white
        tableView.translatesAutoresizingMaskIntoConstraints = false
        
        tableView.topAnchor.constraint(equalTo: self.view.topAnchor)
            .isActive = true
        tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor)
            .isActive = true
        tableView.leftAnchor.constraint(equalTo: self.view.leftAnchor)
            .isActive = true
        tableView.rightAnchor.constraint(equalTo: self.view.rightAnchor)
            .isActive = true
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        
        let nib = UINib(nibName: "TableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "Cell")
        
        tableView.refreshControl = self.refreshControl
        searchBar.showsCancelButton = true
        
        self.navigationItem.rightBarButtonItem = searchButton
        self.searchBar.placeholder = "Enter category"
        
    }
    
    
    private func bindsViewModel() {
        guard let viewModel = self.viewModel else { return }
        
        //Подписка на промежуточный субъект для таблицы
        tableViewData
            .observeOn(MainScheduler.instance)
            .bind(to: self.tableView.rx.items(cellIdentifier: "Cell")) { (index, model, cell) in
                cell.textLabel?.text = model.pubDateString()
                cell.detailTextLabel?.text = model.title
            }
            .disposed(by: disposeBag)
        
        //Биндинг неотфильтрованных новостей
        viewModel.rssItems
        .bind(to: self.tableViewData)
        .disposed(by: disposeBag)
        
        //Подписка на состояние обновления
        //Задержка необходима для более корректной отработки анимации схлопывания refrechControl
        viewModel.refreshing
            .delay(.milliseconds(1000), scheduler: ConcurrentDispatchQueueScheduler.init(qos: .utility))
            .observeOn(MainScheduler.instance)
            .bind(to: self.refreshControl.rx.isRefreshing)
            .disposed(by: disposeBag)
        
        //Биндинг для pull to refresh
        self.refreshControl.rx.controlEvent(.valueChanged)
            .bind(onNext: {
                viewModel.loadRss()
            })
            .disposed(by: disposeBag)
        
        //Биндинг для выбора ячейки
        self.tableView.rx.itemSelected
            .bind(onNext: { [weak self] in
                guard let `self` = self else { return }
                self.viewModel?.selectItem(index: $0.row)
                let vc = InfoViewController.init(nibName: "InfoViewController", bundle: nil)
                vc.viewModel = self.viewModel
                self.navigationController?.pushViewController(vc, animated: true)
            })
            .disposed(by: disposeBag)
        
        //Биндинг для снятия выбра ячейки
        self.tableView.rx.itemDeselected
            .bind(onNext: { [weak self] _ in
                self?.viewModel?.deselectItem()
            })
            .disposed(by: disposeBag)
        
    }
    
    private func bindsUI() {
        
        //Реакция на нажатие кнопки search в navigationItem
        self.searchButton.rx.tap
            .observeOn(MainScheduler.instance)
            .bind(onNext: { [weak self] in
                self?.navigationItem.rightBarButtonItem = nil
                self?.navigationItem.titleView = self?.searchBar
                self?.tableView.refreshControl = nil
            })
            .disposed(by: disposeBag)
        
        //Реакция на нажатие кнопки cancel у serachBar
        self.searchBar.rx.cancelButtonClicked
        .observeOn(MainScheduler.instance)
        .bind(onNext: { [weak self] in
            guard let `self` = self else { return }
            self.navigationItem.rightBarButtonItem = self.searchButton
            self.navigationItem.titleView = nil
            self.tableView.refreshControl = self.refreshControl
            self.searchBar.text = nil
            //Единичное получение полного списка новостей, когда фильтр был отменен
            self.viewModel?.rssItems.take(1)
                .bind(to: self.tableViewData)
                .disposed(by: self.disposeBag)
        })
        .disposed(by: disposeBag)
        
        //Фильтр новостей по категориям
        self.searchBar.rx.text.orEmpty
        .throttle(.milliseconds(1000), scheduler: MainScheduler.instance)
        .distinctUntilChanged()
        .filter({ [weak self] _ in self?.navigationItem.titleView != nil })
        .flatMap({ [weak self] query -> Observable<[NewsVM.Item]> in
            guard let viewModel = self?.viewModel else { return .empty() }
            return viewModel.rssItems.take(1)
                //.do(onNext: { print("source: \($0.count) => \(query)") })
                .flatMap({ Observable<NewsVM.Item>.from($0) })
                .filter({ $0.category.contains(query) })
                .toArray()
                .asObservable()
        })
        //.do(onNext: { print("filtering \($0.count)") })
        .bind(to: tableViewData)
        .disposed(by: disposeBag)
    }
    

}



//
//  InfoViewController.swift
//  testAppVelox
//
//  Created by MAC OS on 08/10/2019.
//  Copyright © 2019 CinereoCardinalis. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class InfoViewController: UIViewController {
    
    //MARK: - Variables
    
    @IBOutlet private var pubDateLabel: UILabel!
    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var fullTextLabel: UILabel!
    
    @IBOutlet private var imageView: UIImageView!
    
    private let disposeBag = DisposeBag()
    
    var viewModel: NewsVM?
    
    //MARK: - View Controller

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureImageView(image: nil)
        bindsViewModel()
    }
    
    //MARK: - Private
    
    private func bindsViewModel() {
        guard let viewModel = self.viewModel else { return }
        
        //Подписка на полечение выбранной новости
        viewModel.currentItem
            .observeOn(MainScheduler.instance)
            .bind(onNext: { [weak self] in
                guard let `self` = self else { return }
                guard let item = $0 else { return }
                self.pubDateLabel.text = item.pubDateString()
                self.titleLabel.text = item.title
                self.fullTextLabel.text = item.fullText
            })
            .disposed(by: disposeBag)
        
        //Подписка на получение изображения выбранной новости
        viewModel.loadImageDataForCurrentItem()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] in
                self?.configureImageView(image: UIImage(data: $0))
            })
            .disposed(by: disposeBag)
    }
    
    //Отображает изображение, если он есть
    private func configureImageView(image: UIImage?) {
        guard let image = image else {
            imageView.isHidden = true
            return
        }
        
        imageView.image = image
        imageView.isHidden = false
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
